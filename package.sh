sudo apt-get install \
     blt-demo distcc icecc clang-7-doc cmake-doc gawk-doc gfortran-multilib gfortran-doc \
     gfortran-8-multilib gfortran-8-doc libgfortran5-dbg git-doc \
     graphviz-doc libasound2-doc libboost-doc libboost1.67-doc gccxml \
     libmpfrc++-dev libntl-dev xsltproc doxygen docbook-xml docbook-xsl default-jdk fop libcairo2-doc \
     libcurl4-doc libidn11-dev libkrb5-dev libldap2-dev librtmp-dev libeigen3-doc libgcrypt20-doc \
     libglib2.0-doc libxml2-utils gmp-doc libgmp10-doc libgraphite2-utils libgtk-3-doc \
     libhwloc-contrib-plugins icu-doc libtool-doc liblzma-doc libmpfr-doc ncurses-doc libomp-7-doc \
     openmpi-doc imagemagick libpango1.0-doc qt5-image-formats-plugins qtwayland5 readline-doc seccomp \
     gcj-jdk libwayland-doc libxaw-doc libxml-sax-expatxs-perl llvm-7-doc opencl-icd python-crypto-doc \
     python-cryptography-doc python3-cryptography-vectors libkf5wallet-bin gir1.2-gnomekeyring-1.0 \
     python-secretstorage-doc python-setuptools-doc python3-tk-dbg default-libmysqlclient-dev firebird-dev \
     libpq-dev libsqlite3-dev unixodbc-dev ri ruby-dev bundler swig-doc swig-examples swig3.0-examples \
     swig3.0-doc tcl-doc debhelper texlive-generic-recommended texinfo-doc-nonfree perl-tk \
     texlive-fonts-recommended-doc texlive-latex-base-doc tk-doc valgrind-mpi kcachegrind alleyoop \
     valkyrie xfsdump attr quota
